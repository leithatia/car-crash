package org.academiadecodigo.carcrash.cars;

public class CarLogic {

    public static void checkForCollision(Car[] cars) {
        System.out.println("Checking for collision");
        for (int i = 0; i < cars.length - 1; i++) {
            System.out.println("isCrashed: " + cars[i].isCrashed());
            for (int j = i + 1; j < cars.length; j++) {
                System.out.println("Is same pos: " + cars[i].isSamePosition(cars[j]));
                if (cars[i].isSamePosition(cars[j])) {
                    cars[i].crashed();
                    cars[j].crashed();
                    System.out.println("cars have crashed");
                }
            }
        }
    }
}
