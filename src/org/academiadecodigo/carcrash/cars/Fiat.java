package org.academiadecodigo.carcrash.cars;

public class Fiat extends Car {

    public Fiat(int speed) {
        super(speed);
    }
    @Override
    public String toString() {
        return "F";
    }
}
