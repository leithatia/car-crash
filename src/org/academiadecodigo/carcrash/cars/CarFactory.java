package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class CarFactory {

    public static  Car getNewCar() {

        if ((int) (Math.random() * 2) == 0) {
            return new Fiat(1);
        } else {
            return new Mustang(3);
        }

    }

}
