package org.academiadecodigo.carcrash.cars;

public class Mustang extends Car {

    public Mustang(int speed) {
        super(speed);
    }
    @Override
    public String toString() {
        return "M";
    }
}
