package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.enums.Direction;
import org.academiadecodigo.carcrash.field.Position;

public abstract class Car {

    /**
     * The position of the car on the grid
     */
    private final Position pos;
    private boolean isCrashed;
    private Direction currentDirection;
    private final int speed;

    public Car(int speed) {
        pos = new Position();
        isCrashed = false;
        currentDirection = Direction.randomDirection();
        this.speed = speed;
    }

    public Position getPos() {
        return pos;
    }

    public void crashed() {
        isCrashed = true;
    }

    public boolean isCrashed() {
        return isCrashed;
    }

    public void drive(Car car, Car[] cars) {
        for (int i = 0; i < speed; i++) {
            changeDirAtRandom();
            CarLogic.checkForCollision(cars);

            if (!car.isCrashed) {
                switch (currentDirection) {
                    case LEFT:
                        pos.moveLeft();
                        break;
                    case RIGHT:
                        pos.moveRight();
                        break;
                    case UP:
                        pos.moveUp();
                        break;
                    case DOWN:
                        pos.moveDown();
                        break;
                }
            }
        }

        if (pos.isOffScreen()) {
            switch (currentDirection) {
                case LEFT:
                    pos.moveRight();
                    pos.toggleOffScreen();
                    currentDirection = Direction.RIGHT;
                    break;
                case RIGHT:
                    pos.moveLeft();
                    pos.toggleOffScreen();
                    currentDirection = Direction.LEFT;
                    break;
                case UP:
                    pos.moveDown();
                    pos.toggleOffScreen();
                    currentDirection = Direction.DOWN;
                    break;
                case DOWN:
                    pos.moveUp();
                    pos.toggleOffScreen();
                    currentDirection = Direction.UP;
                    break;
            }
        }
    }

    private void changeDirAtRandom() {
        int randomChoice = (int) (Math.random() * 10);

        if (randomChoice == 0) {
            Direction newDirection = Direction.randomDirection();

            while (newDirection == currentDirection) {
                newDirection = Direction.randomDirection();
            }

            currentDirection = newDirection;
        }
    }

    public boolean isSamePosition(Car car) {
        System.out.println("This car: " + this.getPos());
        System.out.println("Test car: " + car.getPos());
        return this.getPos().getCol() == car.getPos().getCol() && this.getPos().getRow() == car.pos.getRow();
    }
}
