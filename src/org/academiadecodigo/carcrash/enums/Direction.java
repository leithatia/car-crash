package org.academiadecodigo.carcrash.enums;

public enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN;

    public static Direction randomDirection() {
        Direction[] directions = Direction.values();
        return directions[(int) (Math.random() * directions.length)];
    }
}
