package org.academiadecodigo.carcrash.field;

public class Position {

    private int col;
    private int row;
    private boolean offScreen;

    public Position() {
        this.col = randPos(Field.getWidth());
        this.row = randPos(Field.getHeight());
        offScreen = false;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public boolean isOffScreen() {
        return offScreen;
    }

    public void toggleOffScreen() {
        offScreen = false;
    }

    private int randPos(int maxPos) {
        return (int) (Math.random() * maxPos);
    }

    public void moveLeft() {
        col--;
        if (col < 0) {
            offScreen = true;
        }
    }

    public void moveRight() {
        col++;
        if (col >= Field.getWidth()) {
            offScreen = true;
        }
    }

    public void moveUp() {
        row--;
        if (row < 0) {
            offScreen = true;
        }
    }

    public void moveDown() {
        row++;
        if (row >= Field.getHeight()) {
            offScreen = true;
        }
    }

    @Override
    public String toString() {
        return "Col: " + getCol() + " Row: " + getRow();
    }
}
